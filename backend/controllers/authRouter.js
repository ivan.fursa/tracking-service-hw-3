const express = require('express');
const router = new express.Router();

const { createProfile, login } = require('../services/authService');
const { wrap } = require('../utils/asyncWrapper');
const {
  registrationValidator,
} = require('../middlewares/validationMiddleware');

router.post('/register', registrationValidator, wrap(async (req, res) => {
  const {
    email,
    password,
    role,
  } = req.body;

  await createProfile({ email, password, role });

  res.json({ message: 'Account creeated successfully' });
}));

router.post('/login', wrap(async (req, res) => {
  const {
    email,
    password,
  } = req.body;

  const token = await login({ email, password });

  res.json({ message: 'Success', jwt_token: token });
}));

module.exports = {
  authRouter: router,
};
