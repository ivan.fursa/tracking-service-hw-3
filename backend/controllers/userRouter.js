const express = require('express');
const router = new express.Router();

const { wrap } = require('../utils/asyncWrapper');
const {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
} = require('../services/userService');


router.get('/me', wrap(async (req, res) => {
  const { userId } = req.user;
  const user = await getProfileInfo(userId);

  res.json({ user });
}));

router.delete('/me', wrap(async (req, res) => {
  const { userId } = req.user;

  await deleteProfile(userId);

  res.json({ message: 'Success' });
}));

router.patch('/me', wrap(async (req, res) => {
  const { username } = req.user;
  const {
    oldPassword,
    newPassword,
  } = req.body;

  const token = await changeProfilePassword(username, oldPassword, newPassword);

  res.json({ message: 'Success', jwt_token: token });
}));


module.exports = {
  userRouter: router,
};
