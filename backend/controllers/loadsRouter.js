const express = require('express');
const router = new express.Router();

const { wrap } = require('../utils/asyncWrapper');
const {
  addUserLoad,
} = require('../services/loadsService');
const {
  checkShipperRoleMiddleware,
  // checkDriverRoleMiddleware,
  getloadsByShipper,
  getloadsByDriver,
} = require('../middlewares/validationMiddleware');


router.post('/', checkShipperRoleMiddleware, wrap(async (req, res) => {
  const { userId } = req.user;
  await addUserLoad(userId, req.body);

  res.json({ message: 'Load was created successfully' });
}));

router.get('/', wrap(async (req, res) => {
  const { userId, role } = req.user;
  const offset = req.query.offset;
  const limit = req.query.limit;
  const status = req.query.status;

  if (role.toLowerCase() === 'shipper') {
    const loadsByShipper = await getloadsByShipper(
        userId,
        offset,
        limit,
        status,
    );
    res.json({ loadsByShipper });
  } else {
    const loadsByDriver = await getloadsByDriver(
        userId,
        offset,
        limit,
        status,
    );
    res.json({ loadsByDriver });
  }
}));


module.exports = {
  loadsRouter: router,
};
