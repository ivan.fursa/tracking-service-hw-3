require('dotenv').config();
// just a comment
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const PORT = process.env.PORT || 8080;

const { authMiddleware } = require('./middlewares/authMiddleware');
const { authRouter } = require('./controllers/authRouter');
const { userRouter } = require('./controllers/userRouter');
const { truckRouter } = require('./controllers/truckRouter');
const { loadsRouter } = require('./controllers/loadsRouter');
const { CustomError } = require('./utils/errors');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);


app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', [authMiddleware], userRouter);
app.use('/api/trucks', [authMiddleware], truckRouter);
app.use('/api/loads', [authMiddleware], loadsRouter);


app.use((req, res, next) => {
  res.status(404).json({ message: 'Not found' });
});

app.use((err, req, res, next) => {
  if (err instanceof CustomError) {
    return res.status(err.status).json({ message: err.message });
  }
  res.status(500).json({ message: err.message });
});

const startApp = async () => {
  try {
    await mongoose.connect(process.env.LOCAL_DB_CONNECT || 
      'mongodb+srv://IvanFursa:IvanFursa@clusterivanfursa.wwioe.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
      useNewUrlParser: true, useUnifiedTopology: true,
    });

    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server start: ${err.message}`);
  }
};
startApp();
