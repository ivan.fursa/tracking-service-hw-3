const Joi = require('joi');


const registrationValidator = async (req, res, next) => {
  const Schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .alphanum()
        .min(3)
        .max(40)
        .required(),
    role: Joi.string()
        .valid('SHIPPER', 'DRIVER')
        .insensitive(),
  });

  try {
    await Schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

const roleValidation = (role) => {
  return async (req, res, next) => {
    if (req.user.role.toLowerCase() !== role) {
      res.status(403).json({ message: `User role has to be "${role}"` });
    } else {
      await next();
    }
  };
};
const checkShipperRoleMiddleware = roleValidation('shipper');
const checkDriverRoleMiddleware = roleValidation('driver');


module.exports = {
  registrationValidator,
  checkShipperRoleMiddleware,
  checkDriverRoleMiddleware,
};

