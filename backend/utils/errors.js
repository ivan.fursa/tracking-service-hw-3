/** Creates custom error */
class CustomError extends Error {
  /**
   * @constructor
   * @param {string} message - error message to show
  */
  constructor(message) {
    super(message || 'Invalid request');
    this.status = 500;
  }
}
/** User doesn't have perrnission to do something */
class InvalidRequestError extends CustomError {
  /**
   * @constructor
   * @param {string} message - error message to show
   */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

/** User doesn't have perrnission to do something */
class NotAuthorizedError extends CustomError {
  /**
   * @constructor
   * @param {string} message - error message to show
   */
  constructor(message) {
    super(message || 'Not authorized' );
    this.status = 401;
  }
}

module.exports = {
  InvalidRequestError,
  CustomError,
  NotAuthorizedError,
};
